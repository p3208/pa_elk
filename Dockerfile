FROM node:16 as tsc-builder
WORKDIR /app
COPY . /app/
RUN npm install
RUN npm install -g gulp-cli
RUN gulp

FROM node:16
WORKDIR /app
COPY package*.json /app/
RUN npm install --production
COPY --from=tsc-builder /app/dist /app/dist
EXPOSE $PORT
CMD npm start
