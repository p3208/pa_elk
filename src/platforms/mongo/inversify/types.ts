export const TYPES = {
    Connection: Symbol.for('MongooseConnection'),
    Model: {
        Search: Symbol.for('SearchModel'),
    }
};
