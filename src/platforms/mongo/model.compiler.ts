import {model, Model} from 'mongoose';
import {ISearchDocument, SearchSchema} from '../../definitions';

export class ModelCompiler {
  public static search(): Model<ISearchDocument> {
    return model<ISearchDocument>('Search', SearchSchema, 'search');
  }
}
