import {Document, Model, Query, Mongoose} from 'mongoose';
import {inject, injectable} from 'inversify';
import {ObjectId} from "bson";
import {TYPES} from "../inversify";
import {DAO} from "../../../dao";

@injectable()
export abstract class AbstractMongoDAO<C extends Document> implements DAO<C> {

    abstract get model(): Model<C>;
    readonly mongoose: Mongoose;

    protected constructor(@inject(TYPES.Connection) mongoose: Mongoose) {
        this.mongoose = mongoose;
    }

    async all(limit?: number, offset?: number): Promise<C[]> {
        let query: Query<C[], C> = this.model.find() as Query<C[], C>;
        if (limit !== undefined) {
            query = query.limit(limit);
        }
        if (offset !== undefined) {
            query = query.skip(offset);
        }
        return query.exec();
    }

    public async findById(id: string): Promise<C | null> {
        const idString: any = id;
        if(!ObjectId.isValid(idString)) {
            return null;
        }
        return this.model.findOne({_id: idString})
    }

    public async delete(id: string): Promise<C | null> {
        const idString: any = id;
        if(!ObjectId.isValid(idString)) {
            return null;
        }
        return this.model.findByIdAndDelete(idString);
    }
}
