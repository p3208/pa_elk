import {AbstractMongoDAO} from './abstract-mongo.dao';
import {ISearchDAO} from '../../../dao';
import {Model, Mongoose} from 'mongoose';
import {inject, injectable} from 'inversify';
import {TYPES} from '../inversify';
import {ISearchDocument, ISearch} from '../../../definitions';

@injectable()
export class SearchDAO extends AbstractMongoDAO<ISearchDocument> implements ISearchDAO {

  public constructor(@inject(TYPES.Connection) mongoose: Mongoose,
                     @inject(TYPES.Model.Search) private readonly searchModel: Model<ISearchDocument>) {
    super(mongoose);
  }

  get model(): Model<ISearchDocument> {
    return this.searchModel;
  }
}
