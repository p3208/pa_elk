import {connect, Mongoose} from 'mongoose';

export class ConnectUtils {

  static async open(): Promise<Mongoose> {
    let retry = 0;
    let lastError: Error | undefined = undefined;
    do {
      try {
        return await connect(process.env.MONGO_URI as string, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: true,
          useCreateIndex: true,
          auth: {
            user: process.env.MONGO_USER as string,
            password: process.env.MONGO_PASSWORD as string
          },
          authSource: 'admin'
        });
      } catch (err: any) {
        lastError = err;
        console.error(err);
        await new Promise(((resolve) => setTimeout(resolve, 1000))); // wait 1 second
        retry++;
      }
    } while (retry < 5);
    throw lastError;
  }
}
