import {ToObjectOptions} from 'mongoose';

export class SchemaUtils {

    static toPrettyObjectTransform(ignoredFields: string[] | undefined = undefined): ToObjectOptions {
        return {
            transform: (doc, ret) => {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
                if(ignoredFields !== undefined) {
                    for(let field of ignoredFields) {
                        delete ret[field];
                    }
                }
            }
        }
    }
}
