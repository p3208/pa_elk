import {interfaces} from 'inversify';
import {ConnectUtils} from './utils';
import {Model, Mongoose} from 'mongoose';
import {AsyncUnloaderContainerModule, SHARED_TYPES} from '../../inversify';
import {TYPES} from './inversify';
import {ISearchDAO} from '../../dao';
import {SearchDAO} from './dao';
import {ModelCompiler} from './model.compiler';
import {ISearchDocument} from '../../definitions';

export const mongoModule = new AsyncUnloaderContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind, isBound: interfaces.IsBound): Promise<void> => {
  if (!isBound(TYPES.Connection)) {
    const connection = await ConnectUtils.open();
    bind<Mongoose>(TYPES.Connection).toConstantValue(connection);
    bind<Mongoose>(Mongoose).toConstantValue(connection);
  }

  //Models
  bind<Model<ISearchDocument>>(TYPES.Model.Search).toConstantValue(ModelCompiler.search());

  //DAOs
  if (!isBound(SHARED_TYPES.DAO.Search)) {
    bind<ISearchDAO>(SHARED_TYPES.DAO.Search).to(SearchDAO).inSingletonScope();
  }
}, async (container: interfaces.Container) => {
  if (container.isBound(Mongoose)) {
    const mongoose = container.get<Mongoose>(Mongoose);
    await mongoose.disconnect();
  }
});
