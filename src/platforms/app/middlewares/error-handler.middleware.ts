import {Context} from 'koa';
import {BadRequestError, KoaMiddlewareInterface, Middleware, UnauthorizedError} from 'routing-controllers';
import {ApiError} from '../../../errors';
import {injectable} from "inversify";
import {ValidationError} from 'class-validator';

export type ErrorHandled = { status: number, body: ApiError };

@injectable()
@Middleware({type: 'before'})
export class ErrorHandlerMiddleware implements KoaMiddlewareInterface {

  public static handleValidationErrors(validationErrors: ValidationError[]): ApiError {
    const errorMessage = validationErrors.reduceRight((errorString: string, validationError: ValidationError) => {
      if (!validationError.constraints) return errorString;
      return errorString + Object.values(validationError.constraints).join(', ');
    }, '');
    return new ApiError(400, errorMessage);
  }

  public static handleApiErrors(error: any, context?: Context): ErrorHandled {
    const errorHandled = {
      body: {
        path: '',
        method: '',
      }
    } as ErrorHandled;

    if (ApiError.isApiError(error)) {

      errorHandled.body = error;
      errorHandled.status = error.httpCode;
    } else if (error instanceof BadRequestError) {
      if ((error as unknown as { errors: ValidationError[] }).errors) {
        const validationErrors = (error as unknown as { errors: ValidationError[] }).errors;

        const apiError = ErrorHandlerMiddleware.handleValidationErrors(validationErrors);

        errorHandled.status = apiError.httpCode;
        errorHandled.body = apiError;
      } else {
        const apiError = new ApiError(error.httpCode, error.message);

        errorHandled.status = apiError.httpCode;
        errorHandled.body = apiError;
      }


    } else if (error instanceof UnauthorizedError) {
      const apiError = new ApiError(error.httpCode, error.message);
      errorHandled.status = apiError.httpCode;
      errorHandled.body = apiError;
    }

    if (context) {
      errorHandled.body.path = context.request.url;
      errorHandled.body.method = context.request.method;
    }

    return errorHandled;
  }

  public static printError(error: any): void {
    if (error.errors && error.errors.length > 0 && error.errors[0] instanceof ValidationError) {
      const errors = error.errors as ValidationError[];
      const toPrint = errors.map(error => {
        const constraints = error.constraints as { [type: string]: string };
        return Object.keys(constraints).map(constraintKey => constraints[constraintKey]).join(',');
      }).join(', ');
      console.error(toPrint);
    } else {
      console.error({error});
    }
  }

  public async use(context: Context, next: (err?: any) => Promise<any>): Promise<any> {
    try {
      await next();
    } catch (err) {
      ErrorHandlerMiddleware.printError(err);
      const errorHandled = ErrorHandlerMiddleware.handleApiErrors(err, context);
      context.body = errorHandled.body;
      context.status = errorHandled.status;
    }
  }
}
