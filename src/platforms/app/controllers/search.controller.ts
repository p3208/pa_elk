import {Authorized, CurrentUser, Get, JsonController} from 'routing-controllers'
import {OpenAPI} from 'routing-controllers-openapi'
import {inject, injectable} from 'inversify';
import {SHARED_TYPES} from '../../../inversify';
import {ISearchDAO} from '../../../dao';
import {ISearch, IAmqpService, ISearchService} from '../../../definitions';

@injectable()
@OpenAPI({summary: "Search Routes"})
@JsonController('/search')
export class SearchController {
  // @inject(SHARED_TYPES.DAO.Search)
  // private readonly searchDAO!: ISearchDAO;

  // @inject(SHARED_TYPES.Services.Search)
  // private readonly searchService!: ISearchService;

  constructor(@inject(SHARED_TYPES.Services.Amqp) amqpService: IAmqpService) {
    console.log('amqpService', amqpService);
    amqpService.registerQueue('elk:create', this.create.bind(this));
  }

  @Get('/')
  @OpenAPI({summary: "Return everything that match filter in query"})
  public async me(): Promise<null> {
    return null;
  }

  private async create() {}
}
