import {interfaces} from 'inversify';
import * as http from 'http';
import * as Koa from 'koa';
import * as logger from 'koa-logger';
import {AsyncUnloaderContainerModule, SHARED_TYPES} from '../../inversify';
import {Env, EnvUtils, HttpUtils} from '../../utils';
import {createKoaServer, useContainer} from 'routing-controllers';
import {SearchController} from './controllers';
import {ErrorHandlerMiddleware} from './middlewares';
import {SwaggerService} from "./services";
import Context = interfaces.Context;

declare type Class<T = any> = new (...args: any[]) => T;

function _bindControllers(context: interfaces.Context, controllers: Class[]): Function[] {
  const container = context.container;
  controllers.forEach((controller) => {
    container.bind(controller).to(controller).inSingletonScope();
  });
  useContainer(container);
  return controllers;
}

function _bindMiddlewares(context: interfaces.Context, middlewares: Class[]): Function[] {
  const container = context.container;
  middlewares.forEach((middleware) => {
    container.bind(middleware).to(middleware).inSingletonScope();
  });
  useContainer(container);
  return middlewares;
}

export const appModule = new AsyncUnloaderContainerModule(async (bind: interfaces.Bind) => {
  bind<http.Server>(SHARED_TYPES.Server).toDynamicValue( (context: Context) => {
    const port = process.env.PORT || 3000;
    const appOptions = {
      middlewares: _bindMiddlewares(context, [
        ErrorHandlerMiddleware,
      ]),
      controllers: _bindControllers(context, [
        SearchController,
      ]),

      defaultErrorHandler: false,
      cors: true
    }
    let app: Koa = createKoaServer(appOptions);

    if (process.env.NODE_ENV !== Env.prod) {
      app = SwaggerService(app, appOptions);
    }

    app.use(logger());

    const server = app.listen(port, () => {
      EnvUtils.execOn(() => {
        console.log(`Main service listening on ${port}...`);
      }, Env.dev, Env.prod);
    });
    EnvUtils.execOn(() => {
      server.on('close', () => console.log(`Main service listening on ${port}...`))
    }, Env.dev, Env.prod);
    return server;
  }).inSingletonScope().whenTargetNamed('main');
}, (container: interfaces.Container): Promise<void> => {
  const server = container.getNamed<http.Server>(SHARED_TYPES.Server, 'main');
  return HttpUtils.close(server);
});
