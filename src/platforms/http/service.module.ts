import {interfaces} from 'inversify';
import {SHARED_TYPES, UnloaderContainerModule} from '../../inversify';
import Axios, {AxiosInstance} from 'axios';
import {TYPES} from './inversify';
import {ISearchService, IAmqpService} from "../../definitions";
import {SearchService, AmqpService} from "./services";

export const serviceModule = new UnloaderContainerModule(async (bind: interfaces.Bind) => {
  bindAmqp(bind);
  bindSearch(bind);
  bind<AxiosInstance>(TYPES.PrivateRequest).toConstantValue(createPrivateAxiosRequest());
  bind<ISearchService>(SHARED_TYPES.Services.Search).to(SearchService).inSingletonScope();
});

function bindAmqp(bind: interfaces.Bind) {
  bind(AmqpService).to(AmqpService).inSingletonScope();
  bind<IAmqpService>(SHARED_TYPES.Services.Amqp).toDynamicValue( context => {
    return context.container.get(AmqpService);
  });
}

function bindSearch(bind: interfaces.Bind) {
  bind(SearchService).to(SearchService).inSingletonScope();
  bind<ISearchService>(SHARED_TYPES.Services.Search).toDynamicValue( context => {
    return context.container.get(SearchService);
  });
}

function createPrivateAxiosRequest(): AxiosInstance {
  return Axios.create({
    proxy: false
  });
}
