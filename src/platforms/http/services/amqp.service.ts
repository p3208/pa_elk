import {Channel, connect, Connection, ConsumeMessage} from 'amqplib';
import {injectable} from "inversify";
import {IAmqpService} from "../../../definitions";

@injectable()
export class AmqpService implements IAmqpService{
  private connected: boolean = false;
  private channel: Channel = {} as Channel;

  constructor() {}

  public async registerQueue(queueName: string, callBack: (msg: ConsumeMessage | null) => void): Promise<void> {
    await this.connect();
    await this.channel.assertQueue(queueName, {durable: false});
    await this.channel.consume(queueName, callBack, {noAck: true});
  }

  public async connect(): Promise<void> {
    if (this.connected) {
      return;
    }
    const connection: Connection = await connect(process.env.RABBITMQ_URL as string);
    this.channel = await connection.createChannel();
    this.connected = true;
  }
}
