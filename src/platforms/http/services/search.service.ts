import {AxiosInstance} from 'axios';
import {inject, injectable} from 'inversify';
import {TYPES} from '../inversify';
import {ISearchService} from '../../../definitions';
import {AxiosUtils} from '../utils';
import {ApiError} from '../../../errors';

@injectable()
export class SearchService implements ISearchService {
  private readonly privateRequest: AxiosInstance;
  private readonly baseURL: string;

  public constructor(@inject(TYPES.PrivateRequest) privateRequest: AxiosInstance) {
    this.privateRequest = privateRequest;
    this.baseURL = process.env.SERVICE_URI as string;
  }
}
