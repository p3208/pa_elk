import * as dotenv from 'dotenv';
dotenv.config();
import * as http from 'http';
import {ContainerManager, SHARED_TYPES} from './inversify';
import {appModule, serviceModule, mongoModule} from './platforms';

const containerManager = ContainerManager.getInstance();

async function main(): Promise<http.Server[]> {
  await containerManager.load([
    serviceModule,
  ], [
    appModule,
    // mongoModule
  ]);
  return [
    containerManager.container.getNamed(SHARED_TYPES.Server, 'main')
  ];
}

main().catch(console.error);
