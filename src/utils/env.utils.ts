export enum Env {
    dev = 'development',
    test = 'test',
    prod = 'production'
}

export class EnvUtils {

    public static isDev(): boolean {
        return process.env.NODE_ENV === Env.dev;
    }

    public static isTest(): boolean {
        return process.env.NODE_ENV === Env.test;
    }

    public static isProd(): boolean {
        return process.env.NODE_ENV === Env.prod;
    }

    public static execOn(action: () => void, ... envs: Env[]): boolean {
        for(let env of envs) {
            if(env === process.env.NODE_ENV) {
                action();
                return true;
            }
        }
        return false;
    }
}