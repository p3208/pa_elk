export interface DAO<C> {
    all(limit?: number, offset?: number): Promise<C[]>;
    findById(id: string): Promise<C | null>;
    delete(id: string): Promise<C | null>;
}
