import 'reflect-metadata';
import {Container, interfaces} from 'inversify';
import {AsyncUnloaderContainerModule} from './async-unloader-container.module';
import {UnloaderContainerModule} from './unloader-container.module';

export class ContainerManager {

  private static instance?: ContainerManager;

  public readonly container: interfaces.Container;
  private modules?: (AsyncUnloaderContainerModule | UnloaderContainerModule)[];

  private constructor() {
    this.container = new Container();
  }

  public static getInstance(): ContainerManager {
    if (ContainerManager.instance === undefined) {
      ContainerManager.instance = new ContainerManager();
    }
    return ContainerManager.instance;
  }

  public async load(modules?: UnloaderContainerModule[], asyncModules?: AsyncUnloaderContainerModule[]): Promise<void> {
    if (this.modules !== undefined) {
      return;
    }
    this.modules = [];
    if (modules !== undefined) {
      this.container.load(...modules);
      this.modules.push(...modules);
    }
    if (asyncModules !== undefined) {
      await this.container.loadAsync(...asyncModules);
      this.modules.push(...asyncModules);
    }
  }

  public async unload() {
    if (this.modules === undefined) {
      return;
    }
    await Promise.all(this.modules.map(async (m) => {
      if (m.unload !== undefined) {
        return m.unload(this.container)
      }
    }));
    this.container.unbindAll();
    this.modules = undefined;
  }
}
