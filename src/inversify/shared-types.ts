export const SHARED_TYPES = {
  Server: Symbol.for('Server'),
  DAO: {
    Search: Symbol.for('SearchDAO'),
  },
  Services: {
    Search: Symbol.for('SearchService'),
    Amqp: Symbol.for('AmqpService'),
  },
};
