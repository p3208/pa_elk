import {AsyncContainerModule, interfaces} from 'inversify';

export type AsyncUnloaderContainerCallBack = (container: interfaces.Container) => Promise<void>;

export class AsyncUnloaderContainerModule extends AsyncContainerModule {
  public readonly unload?: AsyncUnloaderContainerCallBack;

  constructor(registry: interfaces.AsyncContainerModuleCallBack, unload?: AsyncUnloaderContainerCallBack) {
    super(registry);
    this.unload = unload;
  }
}
