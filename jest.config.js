module.exports = {
  globals: {
    'ts-jest': {
      tsconfig: './test/tsconfig.json'
    }
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  verbose: true,
  collectCoverage: true,
  testTimeout: 60000,
  setupFiles: [
      './test/load-env.ts'
  ]
};